<?php get_header( ); 
global $more;
?>
				<div class="col-md-9 pull-right">
					<h1 class="post-title">Intencje mszalne</h1>
					<?php 
						if( get_query_var('paged') )
							get_template_part( 'cpt', 'loop' );
						else
							if( have_posts() ) :
								the_post();
								get_template_part( 'content', 'intencje' );
							endif;
					?>			
					<?php ljpl_bootstrap_simple_archive_pagination(); ?>				
				</div>
				<div class="col-md-3">
					<?php get_sidebar( 'intencje' ); ?>
				</div>
<?php get_footer( ); ?>
	