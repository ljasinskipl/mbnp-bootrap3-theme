<aside class="sidebar">
	<img src="<?php bloginfo('template_url') ?>/assets/img/obraz.png" alt="Matka Boża Nieustającej Pomocy" />
	<div class="page-tree">
		<?php
			$pageTreeArgs = array ('child_of' => $post->ID );
			wp_list_pages( $pageTreeArgs );
		?>
	</div>
	<?php // -- tag query for posts related to current page
		if( get_post_meta($post->ID, 'related_tag', true) != '' ): 
	?>
		<?php $mbnpTagQuery = new WP_Query( 'tag=' . get_post_meta($post->ID, 'related_tag', true) ); ?>
		<?php if( $mbnpTagQuery->have_posts() ) : ?>
			<div class="related-posts">
				<h2 class="widget-title">Aktualności</h2>
				<ul class="related-posts">
					<?php while( $mbnpTagQuery->have_posts() ) : $mbnpTagQuery->the_post( ); ?>
						<li><a href="<?php the_permalink( ); ?>"><?php the_title(); ?></a> (<?php the_date( ); ?>)</li>	
					<?php endwhile; ?>
				</ul>				
			</div>

		<?php endif; ?>
		
	<?php endif; ?>
	<?php dynamic_sidebar('sidebar-page');?>	
</aside>
