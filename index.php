<?php get_header( ); ?>
				<div class="col-md-9 pull-right">
					<?php
					if (have_posts()) :
						while (have_posts()) :
							the_post();	
							get_template_part( 'content', 'ogloszenia' );
						endwhile;
					endif;
					?>					
				</div>
				<div class="col-md-3">
					<?php
						if( is_page() )
							get_sidebar( 'page' );
						elseif( is_single() )
							get_sidebar( 'archive' );
						else 
							get_sidebar( );
					?>	
				</div>
<?php get_footer( ); ?>
	