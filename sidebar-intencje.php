<?php
	$mbnpCptQuery = new WP_Query ( 'post_type=intencje&posts_per_page=15' );
?>

<aside class="sidebar">
	<img src="<?php bloginfo('template_url') ?>/assets/img/obraz.png" alt="Matka Boża Nieustającej Pomocy" />


	<?php if( $mbnpCptQuery->have_posts() ): ?>
		<div>
			<h2 class="widget-title">Archiwalne intencje</h2>
				<ul>
					<?php while( $mbnpCptQuery->have_posts() ) : $mbnpCptQuery->the_post(); ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				</ul>
		</div>
	<?php endif; ?>


	<?php dynamic_sidebar('sidebar-intencje');?>	
</aside>
