<?php get_header( ); ?>
<?php
if( is_category() )
	$title = single_cat_title( '', false );
elseif( is_year() )
	$title = 'Archiwum: ' . get_the_time( 'Y' );
else 
	$title = 'Archiwum';
?>
				<div class="col-md-9 pull-right">				
					<h2 class="archive-title"><?php echo $title; ?></h2>
					<?php
					if (have_posts()) :
					   while (have_posts()) :
					   	the_post();
						get_template_part('excerpt', get_post_format( ) );
					   endwhile;
					endif;
					?>
					<?php ljpl_bootstrap_archive_pagination(); ?>					
				</div>
				<div class="col-md-3">
					<?php get_sidebar( 'archive' ); ?>
				</div>
<?php get_footer( ); ?>
	