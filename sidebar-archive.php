<aside class="sidebar">
	<img src="<?php bloginfo('template_url') ?>/assets/img/obraz.png" alt="Matka Boża Nieustającej Pomocy" />
	<div id="archive">
		<h2 class="widget-title">Archiwum</h2>
		<?php echo wp_get_archives( 'type=yearly' ); ?>
	</div>
	<?php dynamic_sidebar('sidebar-archive');?>	
</aside>
