<article <?php post_class( array( ) ); ?>>
	<h1><?php the_title(); ?></h1>
	<div class="content">
		<?php the_content( ); ?>
	</div>
</article>

