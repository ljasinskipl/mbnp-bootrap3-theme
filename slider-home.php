<?php 
	$mbnpFeaturedPosts = new WP_Query ( 'posts_per_page=3' );
	$mbnpCounter = 0;
?>
<div id="myCarousel" class="carousel slide">
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1" class=""></li>
		<li data-target="#myCarousel" data-slide-to="2" class=""></li>
	</ol>
  	<!-- Carousel items -->
  	<div class="carousel-inner">
  		<?php while( $mbnpFeaturedPosts->have_posts() ) : $mbnpFeaturedPosts->the_post(); ?>
  			<?php if( ++$mbnpCounter == 1)
				$class = "active item";
			else
				$class = "item";
			?>
			
			<div class="<?php echo $class; ?>">
				<?php the_post_thumbnail( 'slider-home' ); ?>
				<div class="carousel-caption">
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt( ); ?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
	
  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="icon-prev"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
  </a>
</div>			