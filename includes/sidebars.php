<?php

// #############################################################################
// ### Sidebary

register_sidebar(array(
	'name' => 'Strona główna',
	'id'   => 'home-widgets',
	'description'   => 'Sidebar na stronie głównej',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));

register_sidebar(array(
	'name' => 'Archiwa',
	'id'   => 'archive-widgets',
	'description'   => 'Sidebar na stronie archiwum / aktualności',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));

register_sidebar(array(
	'name' => 'Ogłoszenia',
	'id'   => 'ogloszenia-widgets',
	'description'   => 'Sidebar przy ogłoszeniach duszpasterskich',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));

register_sidebar(array(
	'name' => 'Intencje',
	'id'   => 'intencje-widgets',
	'description'   => 'Sidebar na stronie głównej',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));

register_sidebar(array(
	'name' => 'Strony',
	'id'   => 'page-widgets',
	'description'   => 'Sidebar dla pojedynczych stron',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));

// #############################################################################
// ### Footer

register_sidebar(array(
	'name' => 'stopka 1',
	'id'   => 'footer1-widgets',
	'description'   => 'Lewa stopka',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));
register_sidebar(array(
	'name' => 'stopka 2',
	'id'   => 'footer2-widgets',
	'description'   => 'Środkowa stopka',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));
register_sidebar(array(
	'name' => 'stopka 3',
	'id'   => 'footer3-widgets',
	'description'   => 'Prawa stopka',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-widget-title">',
	'after_title'   => '</h2>'
));