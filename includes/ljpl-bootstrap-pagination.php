<?php

function ljpl_bootstrap_archive_pagination( $query = NULL) {
	global $wp_query;
	if( !$query )
		$query = $wp_query;
	$big = 999999999; // need an unlikely integer
	$pagination = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $query->max_num_pages,
		'type' => 'array'
	) );
	echo '<div class="center-block">';
	echo '<ul class="pagination">';
	foreach( $pagination as $ele )
		echo '<li>' . $ele . '</li>';
	echo '</ul>';
	echo '</div>';
}

function ljpl_bootstrap_simple_archive_pagination ($query = NULL ) {
	global $paged, $wp_query;
	
	if( !$query )
		$query = $wp_query;
	
	$max_page = $query->max_num_pages;

	if ( !$paged )
		$paged = 1;
	
	$nextpage = intval($paged) + 1;
	
	$previous_link = previous_posts( false );
	$next_link = next_posts( $max_page, false );
	
	echo '<ul class="pager">';

	if( $paged > 1 )
		echo '<li class="previous"><a href="' . $previous_link . '">Poprzednia strona</a></li>';
	else
		echo '<li class="previous disabled"><a href="#">Poprzednia strona</a></li>';
	
	if( $nextpage <= $max_page )
		echo '<li class="next"><a href="' . $next_link . '">Następna strona</a></li>';
	else
		echo '<li class="next disabled"><a href="#">następna strona</a></li>';
	
	echo '</ul>';
}

function ljpl_bootstrap_single_pagination( $args = '' ) {
	$defaults = array(
		'before'           => '<p>' . __( 'Pages:' ), 'after' => '</p>',
		'link_before'      => '', 'link_after' => '',
		'next_or_number'   => 'number', 'nextpagelink' => __( 'Next page' ),
		'previouspagelink' => __( 'Previous page' ), 'pagelink' => '%',
		'echo'             => 1
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			$output .= '<ul>';
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				if ( ( $i == $page )) {
					$output .= '<li class="disabled">';
				} else {
					$output .= '<li>';
				}
					$output .= _wp_link_page( $i );
				$output .= $link_before . $j . $link_after;
					$output .= '</a>';
			}
			$output .= '</li>';
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $link_before . $previouspagelink . $link_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $link_before . $nextpagelink . $link_after . '</a>';
				}
				$output .= '</ul>';
				$output .= $after;
			}
		}
	}

	if ( $echo )
		echo $output;

	return $output;
}
