<article <?php post_class( array( 'row', 'excerpt' ) ); ?>>
	<div class="col-md-3 archive-thumbnail">
		<div class="thumbnail-back"></div>
		<div class="img">
			<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'archive-thumbnail' ); ?></a>
		</div>		
	</div>
	<div class="col-md-9">
		<h2 class="article-title"><a href="<?php the_permalink( ); ?>"><?php the_title( ); ?></a></h2>
		<footer class="meta">
			<i>Opublikowano </i> <time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated">
			<?php the_time('j F Y') ?> r.</time>
			<span class="byline author vcard">
				<i>przez </i> <span class="fn"><?php the_author() ?></span>
			</span>
			<?php comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
		</footer>
		
		<?php the_excerpt(); ?>
		
		<p class="readmore"><a href="<?php the_permalink( ); ?>">Czytaj dalej...</a></p>
	</div>
	<div class="clearfix"></div>
</article>

