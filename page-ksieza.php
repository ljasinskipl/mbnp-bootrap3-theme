<?php get_header( ); 
$ksiezaID = get_the_ID();
?>
				<div class="col-md-9 pull-right">
					<?php
					// -------------------------------------------------------------------------------------------------
					// -- Proboszcz

					$args = array(
							'post_type'   => 'page',
							'post_parent' => $ksiezaID, 
							'meta_query'  => array(
								array(
									'key'   => 'funkcja',
									'value' => 'proboszcz'
								),
								array(
									'key'   => 'funkcja_curr',
									'value' => '1'
								)
							)
					);

					$query_prob = new WP_Query ( $args );
					?>

					<h1>ks. proboszcz</h1>
					<?php if( $query_prob -> have_posts() ) : ?>
					<article>
						<?php while( $query_prob->have_posts() ) : $query_prob->the_post(); ?>
							<h2><?php the_title(); ?></h2>
							<div>
								<?php the_content(); ?>
								<div class="clearfix"></div>
							</div>
						<?php endwhile;?>
					</article>
					<?php endif; ?>

					<?php
					// -------------------------------------------------------------------------------------------------
					// -- Wikariusze

					$args = array(
							'post_type'   => 'page',
							'post_parent' => $ksiezaID, 
							'meta_query'  => array(
								array(
									'key'   => 'funkcja',
									'value' => 'wikariusz'
								),
								array(
									'key'   => 'funkcja_curr',
									'value' => '1'
								)
							)
					);

					$query_wik = new WP_Query ( $args );
					?>	

					<?php if( $query_wik -> have_posts() ) : ?>		
						
						<?php if( $query_wik->found_posts == 1 ) : ?>		
							<h1>ks. wikariusz</h1>
						<?php else: ?>
							<h1>księża wikariusze</h1>
						<?php endif; ?>
					
					<article>
						<?php while( $query_wik->have_posts() ) : $query_wik->the_post(); ?>
							<h2><?php the_title(); ?></h2>
							<div>
								<?php the_content(); ?>
								<div class="clearfix"></div>
							</div>
						<?php endwhile;?>
					</article>
					<?php endif; ?>


					<?php
					// -------------------------------------------------------------------------------------------------
					// -- Rezydenci

					$args = array(
							'post_type'   => 'page',
							'post_parent' => $ksiezaID, 
							'meta_query'  => array(
								array(
									'key'   => 'funkcja',
									'value' => 'rezydent'
								),
								array(
									'key'   => 'funkcja_curr',
									'value' => '1'
								)
							)
					);

					$query_wik = new WP_Query ( $args );
					?>	

					<?php if( $query_wik -> have_posts() ) : ?>		
						
						<?php if( $query_wik->found_posts == 1 ) : ?>		
							<h1>ks. wikariusz</h1>
						<?php else: ?>
							<h1>księża wikariusze</h1>
						<?php endif; ?>
					
					<article>
						<?php while( $query_wik->have_posts() ) : $query_wik->the_post(); ?>
							<h2><?php the_title(); ?></h2>
							<div>
								<?php the_content(); ?>
								<div class="clearfix"></div>
							</div>
						<?php endwhile;?>
					</article>
					<?php endif; ?>

					<?php
					// -------------------------------------------------------------------------------------------------
					// -- Wszyscy księża w historii parafii

						$args = array(
							'post_type'   => 'page',
							'post_parent' => $ksiezaID, 
							'orderby'     => 'meta_value_num menu_order',
							'meta_key'    => 'funkcja_od',
							'order'       => 'ASC'
						);
						$query_ksieza = new WP_Query( $args );
					?>					
					<h1>Księża pracujący w naszej parafii</h1>

					<?php if( $query_ksieza->have_posts() ) : ?>
					<ul>
						<?php while( $query_ksieza->have_posts() ) : $query_ksieza->the_post(); ?>
						<li>
							<?php if( get_the_content() != '' ): ?>
								<a href="<?php the_permalink(); ?>">
							<?php endif; ?>
							<?php the_title(); ?>
							
							<?php if( get_post_meta( $post->ID, 'funkcja_od', true ) && get_post_meta( $post->ID, 'funkcja_do', true ) ) : ?>
								(<?php echo get_post_meta( $post->ID, 'funkcja_od', true );?> &minus; <?php echo( get_post_meta( $post->ID, 'funkcja_do', true ) );?>)
							<?php elseif( get_post_meta( $post->ID, 'funkcja_od', true ) && !get_post_meta( $post->ID, 'funkcja_do', true ) ) : ?>
								(od <?php echo get_post_meta( $post->ID, 'funkcja_od', true );?>)
							<?php elseif( !get_post_meta( $post->ID, 'funkcja_od', true ) && get_post_meta( $post->ID, 'funkcja_do', true ) ) : ?>	
								(do <?php echo get_post_meta( $post->ID, 'funkcja_do', true );?>)
							<?php endif; ?>

							<?php if( get_post_meta( $post->ID, 'funkcja_zmarly', true ) ) : ?>
								&#10013;
							<?php endif; ?>
							<?php if( get_the_content() != '' ): ?>
								</a>
							<?php endif; ?>
						</li>
						<?php endwhile; ?>
					</ul>
					<?php endif;?>
				</div>
				<div class="col-md-3">
					<?php
						if( is_page() )
							get_sidebar( 'page' );
						elseif( is_single() )
							get_sidebar( 'archive' );
						else 
							get_sidebar( );
					?>	
				</div>
<?php get_footer( ); ?>
	