<?php get_header( ); ?>

				<div class="col-md-9 pull-right homepage">
					<?php get_template_part( 'slider', 'home' ); ?>		
					<div id="home-quote">
						<blockquote>    
						Kościół to nie tylko dom z kamieni i złota.<br />
						Kościół żywy i prawdziwy – to jest serc wspólnota.
						</blockquote>                       
					</div>
			
			        
			        <div id="home-photo">
			            <img  src="<?php echo get_stylesheet_directory_uri(); ?>/assets//img/kosciol.jpg" />
			        </div>
			        
			        <p class="welcome">Witamy na internetowej stronie parafii pw. Matki Bożej 
			        Nieustającej Pomocy w Mogilnie.</p>
			        <p class="welcome">&nbsp;</p>
			        <p class="welcome">Stara witryna wciąż jest dostępna pod adresem <a href="http://archiwum.parafiambnp.com">http://archiwum.parafiambnp.com</a></p>   									
				</div>
				<div class="col-md-3">
					<?php get_sidebar( 'home' ); ?>
				</div>
<?php get_footer( ); ?>
	