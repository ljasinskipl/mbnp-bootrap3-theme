<?php

define( 'LJPL_BOOTSTRAP_VERSION', '140707' );
// ############################################################################
// ### Navigation

$content_width = 848;

register_nav_menus( array(
	'footer_menu' => 'My Custom Footer Menu',
	'main-nav' => 'Main navigation'
) );

include_once( 'includes/wp_bootstrap_navwalker.php' ); // -- source https://github.com/twittem/wp-bootstrap-navwalker
include_once( 'includes/ljpl-bootstrap-pagination.php' ); // -- source my bootstrap 2.3 base theme

// ############################################################################
// ### Scripts and styles

add_action( 'wp_enqueue_scripts', 'mbnp_bootstrap3_styles' );
if( !function_exists( 'mbnp_bootstrap3_styles' ) ) {
	function mbnp_bootstrap3_styles() {

		// -- main CSS files
		wp_register_style( 'mbnp-bootstrap3-min', get_bloginfo('template_directory') . '/assets/css/bootstrap.min.css' );
		wp_register_style( 'mbnp.bootstrap3-main', get_bloginfo('stylesheet_url'), false, LJPL_BOOTSTRAP_VERSION );

		wp_enqueue_style( 'mbnp-bootstrap3-min' );
		wp_enqueue_style( 'mbnp.bootstrap3-main' );
		
		// -- jQuery - one script to rule them alL	
		wp_enqueue_script( 'jquery' );
		
		wp_enqueue_script( 'bootstrap3-js', get_bloginfo('template_directory') . '/assets/js/bootstrap.js', array( 'jquery' ), '', true);
		// wp_enqueue_script( 'jlpl-bootstrap-js', get_bloginfo('template_directory') . '/assets/js/ljpl-bootstrap.js', '', true);
	
		// -- gravatar hovercards
		// TODO: FIX
		// wp_enqueue_script( 'gprofiles', 'http://s.gravatar.com/js/gprofiles.js', array( 'jquery' ), 'e', true );
	
		wp_deregister_script( 'prototype' );
		//wp_deregister_script( 'scriptacalous' );
	}
}

// ############################################################################
// ### Widget Areas

include( 'includes/sidebars.php' );

		
// ############################################################################
// ### Thumbnail sizes

function image_crop_dimensions($default, $orig_w, $orig_h, $new_w, $new_h, $crop){
    if ( !$crop ) return null; // let the wordpress default function handle this

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);

    $s_x = floor( ($orig_w - $crop_w) / 2 );
    $s_y = floor( ($orig_h - $crop_h) / 2 );

    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
add_filter('image_resize_dimensions', 'image_crop_dimensions', 10, 6);

add_theme_support( 'post-thumbnails' ); 

add_image_size( 'archive-thumbnail', 150, 150, true );
add_image_size( 'slider-home', 792, 500, true );


// ------------------- ogloszenia i intencje

function mbnp_cpt_pagination( $query ) {
	if( is_admin ())
		return $query;
	
	if( isset( $query->query_vars['post_type'] ) && ( $query->query_vars['post_type'] == 'ogloszenia' || $query->query_vars['post_type'] == 'intencje' ) ){
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		if( $paged == 1 ) {
			$limit = 1;
			$offset = 0;
		} else {
			$limit = 20;
			$offset = 1+20*($paged-2);
		}
	    set_query_var('posts_per_archive_page', $limit);
	    set_query_var('posts_per_page', $limit);	
		set_query_var('offset', $offset);
	}
	return;
}

add_filter('pre_get_posts', 'mbnp_cpt_pagination');



		
