			</div> <!-- .row -->
		</div> <!-- .container -->
		<footer id="main-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<aside>							
							<?php dynamic_sidebar('footer1-widgets');?>
							<div class="clearfix"></div>
							<div class="rss-subscribe">
								<a title="subskrybuj kanał‚ rss" href="<?php bloginfo('rss2_url'); ?>"><img src="<?php bloginfo('template_url') ?>/assets/img/rss.png" alt='rss'/></a>
							</div>
						</aside>					
					</div>
					<div class="col-md-4">
						<aside>
							<?php dynamic_sidebar('footer2-widgets');?>	
						</aside>					
					</div>
					<div class="col-md-4">
					<aside>
						<?php dynamic_sidebar('footer3-widgets');?>					
					</aside>				
					</div>



				</div>
				<div class="row">
					<div class="col-md12 copy">
						<p>©2013 Parafia Matki Bożej Nieustającej Pomocy</p>
						<p>Prawa autorskie do materiałów na stronie zastrzeżone, udostępnione są one tylko i wyłącznie do oglądania i użytku domowego zabrania się ich rozpowszechniania.<br></p>
						
						<p>Powered by <a href="http://www.wordpress.org/">Wordpress</a> and <a href="http://www.ljasinski.pl">Studio Multimedialne ljasinski.pl</a>. Wszelkie prawa zastrzeżone.</p>					
					</div>
				</div>				
			</div>			
		</footer>
	<?php wp_footer();?>
	</body>
</html>