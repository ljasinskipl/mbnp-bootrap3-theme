<form role="search" method="get" class="search-form form-horizontal" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<label for="s">Wyszukaj na stronie:</label>
		<input type="search" class="form-control" name="s" id="s" placeholder="Wpisz termin do wyszukania na stronie" />
	</div>
	<button type="submit" class="btn btn-default">Szukaj</button>
</form>