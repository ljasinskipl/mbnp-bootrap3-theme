<!DOCTYPE html>
<html <?php language_attributes( ); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        
        <!-- wp_head -->
        <?php wp_head(); ?>
        <!-- end of wp_head -->
        
    </head>
	<body <?php body_class(); ?>>
		<?php // -- ljpl_bootstrap_beforeheader( ); 
		?>
		<nav class="navbar navbar-inverse" id="main-navigation" role="navigation" data-spy="affix" data-offset-top="1">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only"><?php __( 'Toggle navigation' ); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
				</div>				
				<?php
				    wp_nav_menu( array(
				        'menu'              => 'main-nav',
				        'theme_location'    => 'main-nav',
				        'depth'             => 2,
				        'container'         => 'div',
				        'container_class'   => 'collapse navbar-collapse navbar-ex1-collapse',
				        'menu_class'        => 'nav navbar-nav',
				        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				        'walker'            => new wp_bootstrap_navwalker())
				    );
				?>				
			</div>
		</nav>
		<div class="container" id="main-container">
			<div class="row">