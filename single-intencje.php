<?php get_header( ); ?>
				<div class="col-md-9 pull-right">
					<h1 class="post-title">Ogłoszenia duszpasterskie</h1>
					<?php
					if (have_posts()) :
						while (have_posts()) :
							the_post();	
							get_template_part( 'content', 'intencje' );
						endwhile;
					endif;
					?>					
				</div>
				<div class="col-md-3">
					<?php get_sidebar( 'intencje' ); ?>	
				</div>
<?php get_footer( ); ?>
	